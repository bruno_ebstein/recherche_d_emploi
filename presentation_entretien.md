# Présentation entretien

[CV](https://bruno_ebstein.gitlab.io/cv/cv_bruno_ebstein.html)

## Introduction

### Personnel

Bonjour, je suis dev, j'ai commencé en alternance en 2012 et je suis en CDI depuis 2018

### Découverte et intérêt du dev

J'ai découvert le développement en **seconde**, via un cours de Mesures Physique et Informatique : on nous avait demandé de faire une petite page HTML pour présenter les résultats, j'avais directement accroché ; le fait de taper des choses qui n'avaient pas (encore) de sens pour moi et qui affichaient comme par **magie** des choses à l'écran. J'ai continué de m'informer et m'y intéresser en suivant des **tutoriels** principalement sur le **site du zéro**, puis en faisant des projets persos pendants mon lycée

### Études

J'ai ainsi orienté mes **études** sur le **développement** au sein de l'**ESGI** pour obtenir un master _"Architecture des Logiciels"_

### Craft

Durant mon cursus, j'ai eu la chance d'avoir une **initiation** au **Clean Code** par **Rui Carvalho**, qui est une personne **connue** dans l'univers du craft, ce qui m'a fait découvrir le **Craft**, j'essayais déjà d'avoir des bonnes pratiques mais je ne savais pas qu'il y avait des méthodes formalisées, et tout un "univers" autour du **Craft**

### Meetup

Rui nous a conseillé d'**aller aux Meetups** pour en apprendre plus, ma **curiosité** m'a poussée à suivre ses conseils. L'un de mes préférés étant **Software Crafters**. J'y ai rencontré beaucoup d'Arolliens, c'est à ces occasions que j'ai entendu parlé de la Combe

### La Combe

La **formation**, les valeurs de **Crafts** et mes connaissances d'Arolliens dans les Meetup m'ont donnés envie de rejoindre **La Combe**

### Projets perso et veille

En parallèle de tout ça, j'aime faire des petits **projets** perso notamment en **javascript**, d'autres pour **simplifier** les tâches de développement : en créant des **outils**, en **automatisant** certaines tâches (script, CI / CD), et avoir des **environnements reproductibles** (Nix, container …) ; la plupart sont **open source**
