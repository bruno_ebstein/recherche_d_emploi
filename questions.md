# Questions

* Entreprise : 
* Entretien n° : 
* Date : 
* Heure début : 
* Heure fin : 
* Personnes présentes :
  * 
    * Role : 
    * Ancienneté : 

## Légende

* `M` = `M`ot
* `C` = `C`omportement
* `A` = `A`ction
* `I` = `I`ncohérence

## Impact positif

* Quelle est l'histoire derrière ce produit ?
  * Quelle est la vision de l'entreprise ?
  * Quelle est la mission / l'objectif / le but de l'entreprise ?
  * Pourquoi vouloir avoir un impact positif ?
* Combien de personnes il y a dans l'entreprise ?
* **Comment vous mesurez votre impact sociétal et environnemental ?**
  * Sur quels indicateurs ?
    * [ ] Sociétal
    * [ ] Environnemental
* Comment vérifiez-vous l'accessibilité ?
  * [ ] Audit externe d'accessibilité ?
* **Entre l'intention initiale de la solution et votre réalité produit, est-ce qu'il y a un alignement ? Ou alors vous vous en êtes éloignés ?**
  * Qu'est-ce qui manque pour ré-aligner ?

## Transparence

* Comment se prennent les décisions d'une nouvelle feature ?
  * [ ] user impliqué
  * [ ] UX impliqué
  * [ ] PO impliqué
  * [ ] dev impliqué
  * [ ] QA impliqué
  * [ ] support
  * [ ] direction
* Quel est le modèle économique ?
  * Est-ce que la marge est connue des employés ?
  * Comment se fait le choix des clients ?
* Comment les personnes communiquent entre elles dans l'équipe ?
  * Comment peut-on dire ce qu'il ne va pas ?
  * Comment ça se passe si une personne à un problème sur une tache ?
    * [ ] pair / mob
    * [ ] MP
    * [ ] message channel
    * [ ] attente du daily
    * [ ] attente de la rétro
* Sur quoi vous n'êtes pas d'accord, en général ?
  * Comment vous gérez ce conflit ?

## équipe / constitution / orga

* Combien de personnes il y a dans l'équipe ?
* Composition de l'équipe ?
  * dev
    * 0 front-end
    * 0 back-end
    * 0 full
  * 0 PO
  * 0 UX
  * 0 UI
  * 0 métier
  * 0 ops
  * 0 QA
* Gestion de projet
  * [ ] Kanban ✅
  * [ ] Scrum
    * Quelle est la durée d'un sprint ?
  * [ ] SAFe ❎
  * [ ] Autre
* **Combien de jours par mois vous réunissez-vous en présentiel ?**

## Humain / RH / Manager

* Comment vous faites votre choix entre différents candidats ?
* **Que faites-vous pour le bien-être des collaborateurs ?**
* **Que faites-vous vous pour créer une bonne sécurité psychologique dans l'équipe ?**

## Qualité de développement

* Pour vous la qualité ça signifie quoi ?
  * [ ] dev ✅
  * [ ] respect user ✅
  * [ ] titre de job ❎
  * [ ] responsable qualité ❎
* Vous avez mis en place quoi pour une bonne satisfaction utilisateur ?
  * Est-ce qu'il y a des tests utilisateurs UX ?
* Qu'est-ce qui est le plus important : livrer vite ou livrer du code de qualité ?
  * [ ] Culture de la qualité ✅
  * [ ] Culture du delivery ❎
    * [ ] "on n'a pas le temps" (point bonus) ❎
* Est-ce que les dev interagissent avec les utilisateurs ?
* Quels types de tâches j'aurais à faire ?
* À quoi ressemble une journée type ?
* **Quel sera mon plus gros challenge ?**
* **Si vous aviez plus de temps, qu'est-ce vous feriez en plus ?**
  * [ ] des tests ❎

## Culture du partage

* Faites vous du mob / pair programming ?
  * quelle proportion ?
    * [ ] full
    * [ ] occasionnel
    * [ ] si blocage
    * [ ] pendant l'onboarding
    * [ ] jamais
  * qui ?
    * [ ] dev
      * [ ] junior
      * [ ] senior
      * [ ] tout le monde
    * [ ] user
    * [ ] PO
    * [ ] UX
    * [ ] QA
    * [ ] métier
    * [ ] intra équipe
    * [ ] inter équipes
    * [ ] extra équipes
* Quand avez-vous aidé pour la dernière fois et sur quoi ?
* Quand ont lieu les temps de partage ?
  * [ ] Pendant le temps de travail ✅
    * [ ] Au quotidien
    * [ ] Ritualisé
    * [ ] Occasionnel / quand quelqu'un en a envie
  * [ ] Hors temps de travail ❎
    * [ ] Pause dej
    * [ ] En soirée
* La formation, l'apprentissage ça signifie quoi pour vous ?
  * Ca se matérialise comment ?
    * [ ] pair / mob
    * [ ] catalogue de formations
    * [ ] conférences
* Avez-vous un budget pour les conférences techniques ?
  * Combien ?
  * Quoi ?
    * [ ] billet conférence
    * [ ] transport
    * [ ] logement
    * [ ] nourriture

## Technique

### Code to Prod

* Quel est le temps entre deux mises en production ?
* Est-ce automatisé ?
  * [ ] Continuous Integration
  * [ ] Continuous Delivery
  * [ ] Continuous Deployment
* Pipeline de mise en production par la meme équipe ?
* Production gérée par une autre équipe ?

### FrontEnd

* Est-ce du front-end uniquement ?
* Est-ce qu'il y a de la logique métier à gérer ?
  * Est-ce uniquement de l'agrégation, restitution et mise en forme de données provenant d'API avec de la navigation ?

* Quelles sont les versions minimales des navigateurs à supporter ?
* **Lib / framework** :
  * [ ] React
  * [ ] Vue
  * [ ] Svelte
  * [ ] Angular
* **Langage** :
  * [ ] Vanilla JS
  * [ ] TypeScript
  * [ ] Babel
  * [ ] Elm
  * [ ] ReasonML
* Comment est géré le **state** / gestion d'état
  * [ ] State du composant
  * [ ] Hooks
  * [ ] Redux
* Comment sont gérés les side effects / **effets de bords**
  * [ ] Hooks
  * [ ] Redux Thunk
  * [ ] Redux Saga
* D'autres libraries ?

### BackEnd

* Langage :
* Lib / framework :
* DB :
  * [ ] SQL / relationnel
  * [ ] Document
  * [ ] Graph
  * [ ] Fichier

### API

* [ ] API interne
  * Développées par la meme équipe ?
    * [ ] oui
    * [ ] non
      * Comment se fait la communication avec l'autre équipe ?
        * Gestion de priorité ?
        * Gestion de projets croisée ?
        * Context Mapping TODO lien DDD
* [ ] API externe
  * Context Mapping TODO lien DDD

## Ressenti personnel

* Pourquoi avez-vous décidé de les rejoindre ?
  * Est-ce que la promesse tenue ?
* **Dans votre quotidien, qu'est-ce que vous adorez ?**
* **Si vous aviez une baguette magique, qu'est-ce que vous changeriez ?**

### Solo

* **Est-ce que tu es heureux dans ton entreprise ?**
  * [ ] oui
    * Pourquoi ?
  * [ ] non
    * Pourquoi ?
* Futur ancien
  * Pourquoi tu pars ?
* Ancien
  * Pourquoi t'es parti ?
